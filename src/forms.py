from flask_wtf import FlaskForm
import wtforms as f
from wtforms.validators import DataRequired, Email, EqualTo, ValidationError
from wtforms.fields.html5 import EmailField

from src.models import User


class LoginForm(FlaskForm):
    email = EmailField('Email address', [DataRequired(), Email()])
    password = f.PasswordField('Password', validators=[DataRequired()])
    remember_me = f.BooleanField('Remember me')
    display = ['email', 'password']


class WaterForm(FlaskForm):
    quantity = f.IntegerField('Water quantity (in ml)', validators=[DataRequired()])
    display = ['quantity']


class RegistrationForm(FlaskForm):
    email = f.StringField('Email', validators=[DataRequired(), Email()])
    password = f.PasswordField('Password', validators=[DataRequired()])
    password2 = f.PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = f.SubmitField('Register')


