FROM python:3.7
COPY . .
WORKDIR .
RUN python3.7 -m pip install --upgrade pip
RUN python3.7 -m pip install -r requirements.txt
ENTRYPOINT ["python3.7"]
CMD ["src/app.py"]