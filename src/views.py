import os
from datetime import datetime

from flask import Blueprint, render_template, redirect, url_for, flash, send_from_directory
from flask_login import (login_user, logout_user, current_user)

from src.app import db, app
from src.forms import LoginForm, RegistrationForm, WaterForm
from src.models import User, Water
from src.utils import list2pdf

home = Blueprint('home', __name__)


@home.route('/') #dekorator
def index():
    return render_template("index.html", user=current_user)


@home.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home.user_profile'))

    form = LoginForm() #else
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first() #sprawdzenie czy istnieje użytkownik o danym mailu
        if user is None or not user.check_password(form.password.data):
            flash('Invalid email or password')
            return redirect(url_for('home.login'))  # url_for -> domena + ściezka, np. 127.0.0.1:5000/login

        login_user(user, remember=form.remember_me.data) #else
        return redirect(url_for('home.user_profile'))
    return render_template('login.html', form=form, user=current_user)


@home.route("/logout")
def logout():
    logout_user()
    return redirect('/')


@home.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home.index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('home.login'))

    return render_template('registration.html', title='Register', form=form, user = current_user)


@home.route('/profile', methods=['GET', 'POST'])
def user_profile():
    form = WaterForm()
    if form.validate_on_submit():
        water = Water(quantity=form.quantity.data,
                      user=current_user,
                      timestamp=datetime.now())
        db.session.add(water)
        db.session.commit()
        flash('Congratulations! Your data was successfully added.')
        return redirect(url_for('home.user_profile'))

    water_list = Water.query.filter_by(user_id=current_user.id)
    sum_of_quantities = 0
    for water in water_list:
        sum_of_quantities += water.quantity
    return render_template('profile.html', title='User Profile', water_list=water_list, form=form,
                           user=current_user, sum_of_water=sum_of_quantities)


@home.route('/generate_raport', methods=['GET'])
def generate_raport():
    water_list = Water.query.filter_by(user_id=current_user.id)
    water_json = list2pdf([water.serialize() for water in water_list]) #tworzymy słownik dla każdego obiektu z listy
    uploads = os.path.join(app.root_path, app.config['UPLOAD_FOLDER']) #tworzymy ścieżkę do folderu z raportem
    with open(os.path.join(uploads, 'raport.pdf'), 'wb+') as f: #otwieranie pliku
        f.write(water_json) #zapis do pliku pdf
    return send_from_directory(directory=uploads, filename='raport.pdf', as_attachment=True) #plik jest automatycznie pobierany
