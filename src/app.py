from decouple import config #decouple wyciąga zmienne z pliku .env
from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__) #instancja aplikacji
login = LoginManager(app) #zmienna do logowania

login.login_view = 'login' #definicja widoku logowania
FULL_DB_URL = f"postgresql+psycopg2://" \
              f"{config('DB_USERNAME', default='postgres')}:" \
              f"{config('DB_PASSWORD', default='12345')}@" \
              f"{config('DB_URL', default='127.0.0.1:5432')}/" \
              f"{config('DB_NAME', default='watercontroller')}" #pełna ścieżka do bazy danych
app.config['WTF_CSRF_SECRET_KEY'] = config('WTF_CSRF_SECRET_KEY', default='secret1')
app.config['SECRET_KEY'] = config('SECRET_KEY', default='secret2')
app.config['SQLALCHEMY_DATABASE_URI'] = FULL_DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = 'uploads'
db = SQLAlchemy(app)
login_manager = LoginManager(app)


@login_manager.user_loader
def load_user(id):
    from src.models import User #ten import musi być w tym miejscu żeby nie zapętlać się z innymi importami
    return User.query.filter_by(id=id).first()


def register_blueprints(): #unikamy zapętlenia importów
    from src.views import home
    app.register_blueprint(home)


if __name__ == '__main__':
    register_blueprints()
    db.init_app(app)
    app.run(debug=True)
