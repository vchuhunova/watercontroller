from io import BytesIO
from pdfdocument.document import PDFDocument


def list2pdf(rows): #funkcja do przerobienia listy zserializowanych obiektów do pliku pdf
    f = BytesIO()
    pdf = PDFDocument(f)
    pdf.init_report()
    for item in rows:
        pdf.p(item)
    pdf.generate()
    return f.getvalue()
