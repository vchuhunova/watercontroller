from datetime import datetime

from flask_login import UserMixin
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash

from src.app import db, login #potrzebujemy do stworzenia klas User i Water

from sqlalchemy.inspection import inspect


class User(db.Model, UserMixin):
    _authenticated = True #wszyscy użytkownicy są z góry authenticated
    __tablename__ = 'user_table'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.Unicode(128), nullable=False, unique=True)
    firstname = db.Column(db.Unicode(128))
    lastname = db.Column(db.Unicode(128))
    password = db.Column(db.Unicode(128))
    is_active = db.Column(db.Boolean, default=True)
    is_admin = db.Column(db.Boolean, default=False)
    is_anonymous = False

    def get_id(self):
        return self.id

    def __str__(self):
        return self.email

    def __init__(self, *args, **kw):
        super(User, self).__init__(*args, **kw)
        self._authenticated = True

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @property
    def is_authenticated(self):
        return self._authenticated


def serialize_object(obj, fields): #serializacja - zamiana obiektu na słownik (json)
    d = {}
    for key in [e for e in inspect(obj).attrs.keys() if e in fields]:
        obj_atr = getattr(obj, key)
        if isinstance(obj_atr, datetime):
            d[key] = obj_atr.strftime('%Y-%m-%d %H:%M')
        elif isinstance(obj_atr, User):
            d[key] = str(obj_atr) #odnosi się do magicznej funkcji def __str__
        else:
            d[key] = obj_atr
    return d


class Water(db.Model):
    __tablename__ = 'water_table'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    quantity = db.Column(db.Integer)  # ilosc_wody
    user_id = db.Column(db.Integer, db.ForeignKey('user_table.id'))
    user = relationship('User', foreign_keys=user_id)
    timestamp = db.Column(db.DateTime)

    def serialize(self):
        d = serialize_object(self, ['quantity', 'user', 'timestamp'])
        return d


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
